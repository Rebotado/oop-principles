﻿using System;

namespace Serialization_test
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona person = new Persona();
            person.Name = "Victor";
            person.Age = 21;
            person.Gender = "Male";
            SaveXML(person, "//PersonExample.xml");


            Book book = new Book();
            book.Name = "Random Name";
            book.Author = "Random Author";
            book.Genre = "Random Genre";
            SaveXML(book, "//BookExample.xml");

            Close();
            Console.ReadKey();

        }

        public static void SaveXML<T>(T obj, string fileName)
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + fileName;
            System.IO.FileStream file = System.IO.File.Create(path);
            writer.Serialize(file, obj);
            file.Close();
        }

        [Obsolete("This method is obsolete...")]
        public static void Close()
        {

        }


    }
}
