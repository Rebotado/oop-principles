﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace Serialization_test
{
    [Serializable()]
    public class Book : ISerializable
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Author { get; set; }

        public Book() { }


        public Book(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            Genre = (string)info.GetValue("Genre", typeof(string));
            Author = (string)info.GetValue("Author", typeof(string));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", typeof(string));
            info.AddValue("Genre", typeof(string));
            info.AddValue("Author", typeof(string));
        }
    }



}
