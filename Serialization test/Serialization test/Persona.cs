﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Serialization_test
{
    [Serializable()]
    public class Persona
    {

        public string Name { get; set; }
        public int Age { get; set; }
       [XmlIgnore]
        public string Gender { get; set; }
    }
}
