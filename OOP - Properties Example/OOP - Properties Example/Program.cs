﻿using System;

namespace OOP___Properties_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Jugador jugador1 = new Jugador();
            Jugador jugador2 = new Jugador(50, 50);

            Console.WriteLine($"Vida jugador 1: {jugador1.Health}");
            Console.WriteLine($"Mana jugador 1: {jugador1.Mana}");

            Console.WriteLine($"Vida jugador 2: {jugador2.Health}");
            Console.WriteLine($"Mana jugador 2: {jugador2.Mana}");

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("El jugador 2 gana 50 de vida");
            jugador2.Health += 50;
            Console.WriteLine($"Vida jugador 2: {jugador2.Health}");

            Console.WriteLine("\nEl jugador 1 pierde 30 de vida");
            jugador1.Health -= 30;
            Console.WriteLine($"Vida jugador 1: {jugador1.Health}");

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("\nEl jugador 2 pierde 300 millones de vida");
            jugador2.Health -= 300000000;
            Console.WriteLine($"Vida jugador 2: {jugador2.Health}");

            Console.WriteLine("\nEl jugador 1 gana 4000 de vida");
            jugador1.Health += 4000;
            Console.WriteLine($"Vida jugador 1: {jugador1.Health}");

            Console.ReadKey();
        }
    }

    class Jugador
    {
        private int health;
        private int mana;

        public Jugador(int health = 100, int mana = 100) //Constructor, en caso de no ingresar parametros en la creacion del objeto sus valores son 100 por defecto.
        {
            Health = health;
            Mana = mana;
        }

        public int Health
        {
            get { return health; }
            set
            {
                if(value > 100)
                {
                    health = 100;
                }
                else if(value < 0)
                {
                    health = 0;
                }
                else
                {
                    health = value;
                }
            }
        }

        public int Mana
        {
            get { return mana; }
            set
            {
                if (value  > 100)
                {
                    mana = 100;
                }
                else if (value < 0)
                {
                    mana = 0;
                }
                else
                {
                    mana = value;
                }
            }
        }

    }
}
