﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;

namespace Serializer
{
    [Serializable()]
    public class Berenjena : ISerializable

    {
        public int Id { get; set; }
        public string Color { get; set; }
        public string Ricura { get; set; }
        public string Tamaño { get; set; }
        public int Precio { get; set; }
        public string Gozable { get; set; }
        public string THICNESS { get; set; }

        public Berenjena() { }

        public void Registrar()
        {
            Console.Write("Id: ");
            Id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Color:");
            Color = Console.ReadLine();
            Console.Write("Ricura: ");
            Ricura = Console.ReadLine();
            Console.Write("Tama;o: ");
            Tamaño = Console.ReadLine();
            Console.Write("Precio: ");
            Precio = Convert.ToInt32(Console.ReadLine());
            Console.Write("Gozable: ");
            Gozable = Console.ReadLine();
            Console.Write("THICNESS: ");
            THICNESS = Console.ReadLine();

        }

        public override string ToString()
        {
            return $"Id: {Id}\nColor: {Color}\nRicura: {Ricura}\nTama;o: {Tamaño}\nPrecio: {Precio}\nGozable: {Gozable}\nTHICNESS: {THICNESS}";
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Id", Id);
            info.AddValue("Color", Color);
            info.AddValue("Ricura", Ricura);
            info.AddValue("Tamaño", Tamaño);
            info.AddValue("Precio", Precio);
            info.AddValue("Gozable", Gozable);
            info.AddValue("THICNESS", THICNESS);
        }

        public Berenjena(SerializationInfo info, StreamingContext ctxt)
        {
            Id = (int)info.GetValue("Id", typeof(int));
            Color = (string)info.GetValue("Color", typeof(string));
            Ricura = (string)info.GetValue("Ricura", typeof(string));
            Tamaño = (string)info.GetValue("Tamaño", typeof(string));
            Precio = (int)info.GetValue("Precio", typeof(int));
            Gozable = (string)info.GetValue("Gozable", typeof(string));
            THICNESS = (string)info.GetValue("THICNESS", typeof(string));

        }
    }
}
