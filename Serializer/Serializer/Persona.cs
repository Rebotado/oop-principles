﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;

namespace Serializer
{
    [Serializable()]
    public class Persona : ISerializable
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
        public string NumeroTelefono { get; set; }
        public string Email { get; set; }

        public Persona() { }

        public override string ToString()
        {
            return $"Id: {Id}\nNombre: {Nombre}\nApellido: {Apellido}\nEdad: {Edad}\nNumeroTelefono: {NumeroTelefono}\nEmail: {Email}";
        }
        public void Registrar()
        {
            Console.Write("Ingrese Id: ");
            Id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Ingrese el nombre: ");
            Nombre = Console.ReadLine();
            Console.Write("Ingrese el apellido: ");
            Apellido = Console.ReadLine();
            Console.Write("Ingrese la edad: ");
            Edad = Convert.ToInt32(Console.ReadLine());
            Console.Write("Ingrese el numero de telefono: ");
            NumeroTelefono = Console.ReadLine();
            Console.Write("Ingrese el Email: ");
            Email = Console.ReadLine();
        }

        //Metodos para la serializacion

            //Serialize
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Id", Id);
            info.AddValue("Nombre", Nombre);
            info.AddValue("Apellido", Apellido);
            info.AddValue("Edad", Edad);
            info.AddValue("NumeroTelefono", NumeroTelefono);
            info.AddValue("Email", Email);
        }

        //Deserialize
        public Persona(SerializationInfo info, StreamingContext ctxt)
        {
            Id = (int)info.GetValue("Id", typeof(int));
            Nombre = (string)info.GetValue("Nombre", typeof(string));
            Apellido = (string)info.GetValue("Apellido", typeof(string));
            Edad = (int)info.GetValue("Edad", typeof(string));
            NumeroTelefono = (string)info.GetValue("NumeroTelefono", typeof(string));
            Email = (string)info.GetValue("Email", typeof(string));
        }
    }
}
