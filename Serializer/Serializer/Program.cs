﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;


namespace Serializer
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persona> test = Load<List<Persona>>("Persona.xml");
            List<Berenjena> berenjenas = Load<List<Berenjena>>("Berenjenas.xml");



            if (berenjenas == null)
            {
                berenjenas = new List<Berenjena>();
            }

            foreach (Berenjena berenjena in berenjenas)
            {
                Console.WriteLine(berenjena.ToString());
                Console.WriteLine("-------------");


            }


            berenjenas.Add(new Berenjena());
            berenjenas[berenjenas.Count - 1].Registrar();

            Save("Persona.xml", test);
            Save("Berenjenas.xml", berenjenas);
            Console.ReadKey(true);


        }


        public static void Save<T>(string filename, T file)
        {
            string path = Directory.GetCurrentDirectory();
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (Stream tw = new FileStream(path + @"\" + filename, FileMode.Create,FileAccess.Write,FileShare.None))
            {
                serializer.Serialize(tw, file);
                tw.Close();
            }
        }

        public static T Load<T>(string filename)
        {
            if (!File.Exists(filename))
            {
               return default(T);
            }

            string path = Directory.GetCurrentDirectory();
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (FileStream reader = File.OpenRead(path + @"\" + filename))
            {
                object obj = deserializer.Deserialize(reader);
                reader.Close();
                return (T)obj;
            }
        }

    }
}
