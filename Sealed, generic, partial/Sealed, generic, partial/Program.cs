﻿using System;

namespace Sealed__generic__partial
{
    class Program
    {
        static void Main(string[] args)
        {
            int test = 1;
            string testString = "1";
            PartialClass.DoSomething();
            PartialClass.DoSomething2();

            GenericClass<int>.DoSomething(test);
            GenericClass<int>.DoSomething2(testString);
        }
    }
}
