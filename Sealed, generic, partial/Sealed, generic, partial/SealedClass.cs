﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sealed__generic__partial
{
    sealed class A
    {
        public virtual void DoSomething() { }
    }


    class B : A
    {
        public sealed override void DoSomething()
        {
            base.DoSomething(){ }
        }


    }

    class C : B
    {
        
        {
            base.DoSomething();
        }
    }
}
