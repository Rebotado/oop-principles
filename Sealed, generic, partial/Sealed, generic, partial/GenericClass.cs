﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sealed__generic__partial
{
    class GenericClass<T>
    {
        public static void DoSomething(T someValue) { }
        public static void DoSomething2<U>(U somevalue) { }
    }
}
