﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Events_and_delegates
{
    class Achievements
    {
        public Player player { get; set; }

        public void Start(Player player)
        {
            this.player = player;
            this.player.OnEnemyKillEvent += Killed10EnemiesAtTheSameTime; //Al crearse el sistema de logros, el logro Killed10EnemiesAtTheSameTime se suscribe al evento OnEnemyKillEvent de su respectivo jugador.
            this.player.OnEnemyKillEvent += Killed100EnemiesAtTheSameTime;
        }

        public void Killed10EnemiesAtTheSameTime(int enemiesKilled)
        {
            if(enemiesKilled >= 10)
            {
                Console.WriteLine($"{player.Name} has unlocked \"BRUTALITY\":");
                Console.WriteLine("Congratulations! You've killed 10 enemies at the same time");
                Console.WriteLine("-------------------\n");
                player.OnEnemyKillEvent -= Killed10EnemiesAtTheSameTime;
            }

        }

        public void Killed100EnemiesAtTheSameTime(int enemiesKilled)
        {
            if (enemiesKilled >= 100)
            {
                Console.WriteLine($"{player.Name} has unlocked \"HACKER\":");
                Console.WriteLine("Congratulations! You've killed 100 enemies at the same time");
                Console.WriteLine("-------------------\n");
                player.OnEnemyKillEvent -= Killed10EnemiesAtTheSameTime;
            }
        }

    }
}
