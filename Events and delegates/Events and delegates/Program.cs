﻿using System;

namespace Events_and_delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            ConsoleKeyInfo key;
            ///////
            Player player1 = new Player();
            Player player2 = new Player();

            player1.Spawn("Jugador 1");
            player2.Spawn("Jugador 2");

            while (true)
            {
                key = Console.ReadKey();
                if(key.Key == ConsoleKey.Spacebar)
                {
                    player1.Attack(100);
                    player2.Attack(10);
                }
                else if(key.Key == ConsoleKey.Escape)
                {
                    player1.OnKill();
                    break;
                }
            }
            */
            Test test = new Test();
            test.Start();
            test.Update(10);
            test.Update(1);
            Console.ReadKey();
        }
    }
}
