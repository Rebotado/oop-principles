﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Events_and_delegates
{
    class Test
    {
        public delegate void onTest(int number);
        public delegate int OnTestInt(string name);

        public event onTest onTestEvent;
        public event onTest onTestEvent2;
        public event onTest onTestEvent3;
        public event onTest onTestEvent4;
        public event onTest onTestEvent5;



        public void Start()
        {
            onTestEvent += metodo1;
            onTestEvent += metodo2;
            onTestEvent += metodo3;
            onTestEvent2 += MetodoRandom;
        }

        public int MetodoRandom(string name)
        {
            return 0;
        }

        public void metodo1(int number)
        {
            Console.WriteLine($"Callback desde el metodo 1: {number}");
        }

        public void metodo2(int number)
        {
            Console.WriteLine($"Callback desde el metodo 2: {number}");
        }
        public void metodo3(int number)
        {
            Console.WriteLine($"Callback desde el metodo 3: {number}");
        }

        public void Update(int number)
        {
            onTestEvent(number);
            onTestEvent -= metodo2;
        }



    }
}
