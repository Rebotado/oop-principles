﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Events_and_delegates
{
    class Player : Entity
    {
        public delegate void OnEnemyKill(int count);
        public event OnEnemyKill OnEnemyKillEvent;

        public string Name { get; set; }


        public int EnemeiesKilled { get; set; }
        public void Spawn(string name)
        {
            Name = name;
            Achievements ach = new Achievements(); //A; spawnear un jugador se crea un sistema de logros
            ach.Start(this);
            OnEnemyKillEvent += EnemyCounter; //El metodo EnemyCounter se suscribe al evento OnEnemiKillEvent

        }
        public override void OnKill()
        {
            Console.Clear();
            OnEnemyKillEvent -= EnemyCounter; //Al morir el jugador, el metodo EnemyCounter se desuscribe del evento OnEnemyKill
            Console.WriteLine("You're dead. Press any key to restart from checkpoint");
            Console.ReadKey();
        }
        public void EnemyCounter(int count)
        {
            EnemeiesKilled += count;
            Console.WriteLine($"{Name} ------");
            Console.WriteLine($"Enemies killed in last combo: {count}");
            Console.WriteLine($"Enemies killed in total: {EnemeiesKilled}");
            Console.WriteLine("---------");
        }
        public void Attack(int damage)
        {
            int EnemiesKilled = 0;
           for(int i = 0; i < damage; i += 10)
            {
                EnemiesKilled += 1;
            }
            OnEnemyKillEvent(EnemiesKilled);
        }
    }
}
