﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Events_and_delegates
{
    abstract class Entity
    {
        public int Health { get; set; }
        public int Mana { get; set; }
        public int AtackValue { get; set; }
        public int DefenseValue { get; set; }
        public List<Ability> Abilities { get; set; }

        public abstract void OnKill();
        public void OnDamage(int damage)
        {
            Console.WriteLine($"Received {damage} damage");
            Console.WriteLine($"Current Health: {Health}");
        }


    }
}
