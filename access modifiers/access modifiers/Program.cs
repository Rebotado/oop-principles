﻿using System;
using ClassLibrary1;
namespace access_modifiers
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }

        protected internal static void MethodProtectedInternal()
        {

        }

        protected static void MethodProtected()
        {

        }

        private static void MethodPrivate()
        {

        }

        internal static void MethodInternal()
        {

        }
    }
}
