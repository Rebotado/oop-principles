﻿using System;
using System.Collections.Generic;
using System.Text;

namespace access_modifiers
{
    public class Internal
    {
        protected internal static void MethodProtectedInternal()
        {

        }

        protected static void MethodProtected()
        {

        }

        private static void MethodPrivate()
        {

        }

        internal static void MethodInternal()
        {

        }

    }
}
