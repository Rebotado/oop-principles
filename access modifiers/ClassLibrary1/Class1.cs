﻿using System;

namespace ClassLibrary1
{
    public class Class1
    {
        protected internal static void MethodProtectedInternal()
        {
   
        }

        protected static void MethodProtected()
        {

        }

        private static void MethodPrivate()
        {

        }

        internal static void MethodInternal()
        {

        }


    }
}
