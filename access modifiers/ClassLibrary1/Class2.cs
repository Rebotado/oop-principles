﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    class Class2
    {
        protected internal static void MethodProtectedInternal()
        {

        }

        protected static void MethodProtected()
        {

        }

        private static void MethodPrivate()
        {

        }

        internal static void MethodInternal()
        {

        }

    }
}
