﻿using System;

namespace Stream_022019
{
    class Program
    {
        static void Main(string[] args)
        {
            Cuadrilatero cuadrado1 = new Cuadrilatero();
            Cuadrilatero rombo1 = new Rombo();

            cuadrado1.Lado1 = 10;
            cuadrado1.Lado2 = 10;
            cuadrado1.Lado3 = 10;
            cuadrado1.Lado4 = 10;

            rombo1.Lado1 = 15;
            rombo1.Lado2 = 15;
            rombo1.Lado3 = 15;
            rombo1.Lado4 = 15;
            rombo1.Diagonal1 = 4;
            rombo1.Diagonal2 = 3;



            Console.WriteLine($"Area del cuadrado: {cuadrado1.Area()}");
            Console.WriteLine($"Perimetro del cuadrado: {cuadrado1.Perimetro()}");

            Console.WriteLine("-----------------------");

            Console.WriteLine($"Area del Rombo: {rombo1.Area()}");
            Console.WriteLine($"Perimetro del Rombo: {rombo1.Perimetro()}");

            Console.ReadKey();


        }







    }


    //Encapsulamiento
    class Persona
    {

        // persona.Nombre = "Victor";
        private string nombre;
        private string apellidos;
        private int clave;

        public string Nombre //Solo lectura y escritura
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellidos //Solo lectura
        {
            get { return apellidos; }
        }

        public int Clave //Solo escritura
        {
            set { clave = value; }
        }
    }

    //Herencia y polimorfismo

    class Cuadrilatero
    {
        //auto propiedades
        public int Lado1 { get; set; } 
        public int Lado2 { get; set; }
        public int Lado3 { get; set; }
        public int Lado4 { get; set; }
        public int Diagonal1 { get; set; }
        public int Diagonal2 { get; set; }

        public virtual double Area() //Polimorfismo
        {
            return Lado1 * Lado2;
        }

        public int Perimetro()
        {
            return Lado1 + Lado2 + Lado3 + Lado4;
        }        
    }
    
    class Rombo : Cuadrilatero //Herencia
    {
        public override double Area() // Polimorfismo
        {
            return (Diagonal1 * Diagonal2) / 2;
        }
    }
}
