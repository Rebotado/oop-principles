﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractANDInterfaces
{
    class Square
    {
        //Un metodo estatico pertenece al tipo de objeto
        // Un metodo no estatico pertenece al objeto y no al tipo

        public int side1 { get; set; }
        public int side2 { get; set; }
        public int side3 { get; set; }
        public int side4 { get; set; }

        public Square(int side1, int side2, int side3, int side4)
        {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
            this.side4 = side4;
        }

        public static int StaticArea(int side1, int side2)
        {
            return side1 * side2;
        }

        public int NonStaticArea()
        {
            return side1 * side2;
        }



    }
}
