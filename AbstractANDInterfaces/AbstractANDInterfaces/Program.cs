﻿using System;

namespace AbstractANDInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Square cuadrado = new Square(5,5,5,5);
            Console.WriteLine($"Area: {cuadrado.NonStaticArea()}");
            Console.WriteLine($"Area 2: {Square.StaticArea(2, 2)}");
            Console.ReadKey();
        }
    }
}
