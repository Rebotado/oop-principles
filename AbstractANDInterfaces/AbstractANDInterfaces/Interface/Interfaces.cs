﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractANDInterfaces
{
    //NOTA: no se deben poner multiples interfaces en el mismo archivo
    //esto es solo para hacer el ejemplo mas ameno de ver.

    interface Idamageable //Puede recibir da;o
    {
        void Damage(int damageAmount);
    }

    interface Imoveable //puede moverse
    {
        float Speed { get; set; }
        void GetSpeed();
    }

    interface Ikillable //puede morir
    {
        void Die();
    }

    interface Ihealable<T> //puede curarse
    {
        void Heal(T healAmount);
    }

    interface Ienrageable //puede puede entrar en enrage
    {
        bool IsEnraged { get; set; }
    }

    interface Istats
    {
        int Vida { get; set; }
        int Mana { get; set; }
        int DefenseValue { get; set; }
        int AtakValue { get; set; }
    }

}
