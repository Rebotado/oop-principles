﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractANDInterfaces
{
    class Vehicle : Imoveable, Ikillable, Istats
    {
        public float Speed { get; set; }
        public int Vida { get; set; }
        public int Mana { get; set; }
        public int DefenseValue { get; set; }
        public int AtakValue { get; set; }

        public void Die()
        {
            Console.WriteLine("The vehicle has been destroyed");
        }

        public void GetSpeed()
        {
            Console.WriteLine($"Current Speed: {Speed}");
        }
    }
}
