﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractANDInterfaces
{
    class FinalBoss : Ihealable<double>, Ikillable, Ienrageable, Istats
    {
        private bool isEnranged;
        public bool IsEnraged { get; set; }
        public int Vida { get; set; }
        public int Mana { get; set; }
        public int DefenseValue { get; set; }
        public int AtakValue { get; set; }


        public void Die()
        {
            Console.WriteLine("Congratulations! You have finished the game ");
        }

        public void Heal(double healAmount)
        {
           double heal = healAmount;
           if(isEnranged == true)
            {
                Vida += Convert.ToInt32(heal * 10);
            }
            else
            {
                Vida += Convert.ToInt32(heal);
            }
        }
    }
}
