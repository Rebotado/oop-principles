﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractANDInterfaces
{
    class Player2Example : Idamageable, Imoveable, Ihealable<int>
    {
        private float speed;
        public float Speed
        {
            get { return speed; }
            set
            {
                if(value > 10)
                {
                    speed = 10;
                }
                else if(value < 0)
                {
                    speed = 0;
                }
                else
                {
                    speed = value;
                }
            }
        }

        public void Damage(int damageAmount)
        {
            Console.WriteLine($"Player has received {damageAmount} damage");
        }

        public void GetSpeed()
        {
            Console.WriteLine($"Current Speed: {Speed}");
        }

        public void Heal(int healAmount)
        {
            Console.WriteLine($"Player has healed {healAmount}");
        }
    }
}
