﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shitpost_031419
{
    class MenuConsola
    {
        public enum Opciones
        {
            Sumar = 1,
            Restar = 2,
            Multiplicar = 3,
            Dividir = 4,
            Salir = 5
        }

        public void Iniciar()
        {
            Console.WriteLine("Eliga una opcion:\n" +
                "1. Sumar\n" +
                "2. Restar\n" +
                "3. Multiplicar\n" +
                "4. Dividir\n" +
                "5. Salir");
            int seleccion = Convert.ToInt32(Console.ReadLine());
            Seleccion(seleccion);
        }

        public void Seleccion(int seleccion)
        {
            Console.Clear();
            switch (seleccion)
            {
                case (int)Opciones.Sumar:
                    break;
                case (int)Opciones.Restar:
                    break;
                case (int)Opciones.Multiplicar:
                    break;
                case (int)Opciones.Dividir:
                    break;
                case (int)Opciones.Salir:
                    Environment.Exit(1);
                    break;
            }
        }

    }
}
